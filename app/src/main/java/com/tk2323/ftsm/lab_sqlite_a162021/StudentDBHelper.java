package com.tk2323.ftsm.lab_sqlite_a162021;


import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import static com.tk2323.ftsm.lab_sqlite_a162021.StudentContract.*;

public class StudentDBHelper extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 1;
    static final String DATABASE_NAME = "StudentDataBase.db";

    private static final String SQL_CREATE_STUDENTS_TABLE =
            "CREATE TABLE " + StudentEntry.TABLE_NAME + " (" +
                    StudentEntry._ID + " INTEGER PRIMARY KEY, " +
                    StudentEntry.COLUMN_MATRIX_NO + " TEXT NOT NULL, " +
                    StudentEntry.COLUMN_NAME + " TEXT NOT NULL, " +
                    StudentEntry.COLUMN_CITY + " TEXT NOT NULL " +
                    " );";

    public StudentDBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_STUDENTS_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + StudentEntry.TABLE_NAME);
        onCreate(db);
    }
}
