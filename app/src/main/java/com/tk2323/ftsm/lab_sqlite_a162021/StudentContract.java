package com.tk2323.ftsm.lab_sqlite_a162021;

import android.provider.BaseColumns;

public class StudentContract {

    public StudentContract() {

    }

    public static final class StudentEntry implements BaseColumns
    {
        public static final String TABLE_NAME = "students";

        public static final String COLUMN_MATRIX_NO = "matrix_no";
        public static final String COLUMN_NAME = "name";
        public static final String COLUMN_CITY = "city";

    }
}
