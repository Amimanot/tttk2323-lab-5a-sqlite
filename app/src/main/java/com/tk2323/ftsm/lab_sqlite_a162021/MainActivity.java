package com.tk2323.ftsm.lab_sqlite_a162021;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.google.android.material.textfield.TextInputEditText;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

import java.util.ArrayList;

import static com.tk2323.ftsm.lab_sqlite_a162021.StudentContract.*;

public class MainActivity extends AppCompatActivity {

    StudentDBHelper mStudentDBHelper;
    SQLiteDatabase db;
    Button btn_save, btn_update, btn_delete;
    TextInputEditText et_name, et_matrix, et_city;
    ListView lv_student;
    String studentMatrixNo, studentName, studentCity;
    ArrayList<String> studentsInfo;

    private String[] allColumns = {StudentEntry._ID, StudentEntry.COLUMN_MATRIX_NO, StudentEntry.COLUMN_NAME, StudentEntry.COLUMN_CITY};

    @Override
    protected void onResume() {
        super.onResume();
        loadData();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mStudentDBHelper = new StudentDBHelper(MainActivity.this);

        db = mStudentDBHelper.getWritableDatabase();

        btn_save = findViewById(R.id.main_btn_save);
        btn_update = findViewById(R.id.main_btn_update);
        btn_delete = findViewById(R.id.main_btn_delete);

        et_name = findViewById(R.id.main_et_name);
        et_matrix = findViewById(R.id.main_et_matrix);
        et_city = findViewById(R.id.main_et_city);

        lv_student = findViewById(R.id.main_lv_student);

        btn_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                studentMatrixNo = et_matrix.getText().toString();
                studentName = et_name.getText().toString();
                studentCity = et_city.getText().toString();

                ContentValues values = new ContentValues();

                values.put(StudentEntry.COLUMN_MATRIX_NO, studentMatrixNo);
                values.put(StudentEntry.COLUMN_NAME, studentName);
                values.put(StudentEntry.COLUMN_CITY, studentCity);

                insertData(db, values);
            }
        });

        btn_update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ContentValues values = new ContentValues();
                values.put(StudentEntry.COLUMN_CITY, et_city.getText().toString());
                String updateWhereClause = StudentEntry.COLUMN_MATRIX_NO + " LIKE ?";
                String[] updateWhereArgs = new String[]{et_matrix.getText().toString()};

                int affectedRow = db.update(StudentEntry.TABLE_NAME, values, updateWhereClause, updateWhereArgs);

                if (affectedRow > 0)
                {
                    Message.message(getApplicationContext(), affectedRow + " record updated");
                    loadData();
                }
            }
        });

        btn_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String deleteWhereClause = StudentEntry.COLUMN_MATRIX_NO + " LIKE ?";
                String[] deleteWhereArgs = new String[] {et_matrix.getText().toString()};
                int affectedRow = db.delete(StudentEntry.TABLE_NAME, deleteWhereClause, deleteWhereArgs);

                if (affectedRow > 0)
                {
                    Message.message(getApplicationContext(), affectedRow + " record deleted");
                    loadData();
                }
            }
        });
    }

    public void insertData(SQLiteDatabase db, ContentValues values)
    {
        long newRowID = db.insert(StudentEntry.TABLE_NAME, null, values);

        if (newRowID != -1)
        {
            Message.message(MainActivity.this, "new Data Inserted");
            loadData();
        }
    }

    public void loadData()
    {
        studentsInfo = new ArrayList<String>();

        Cursor cursor = db.query(StudentEntry.TABLE_NAME, allColumns, null, null, null, null,null, null);

        cursor.moveToFirst();

        while(!cursor.isAfterLast())
        {
            studentsInfo.add(cursor.getString(0) + ": " +
                    cursor.getString(1) + " Name: " +
                    cursor.getString(2) + " From: " +
                    cursor.getString(3) + " Count: " +
                    cursor.getCount());

            cursor.moveToNext();
        }

        ArrayAdapter<String> adapter = new ArrayAdapter<>(getApplicationContext(), android.R.layout.simple_list_item_1, studentsInfo);

        lv_student.setAdapter(adapter);

        cursor.close();
    }
}
