package com.tk2323.ftsm.lab_sqlite_a162021;

import android.content.Context;
import android.widget.Toast;

public class Message {

    public static void message (Context context, String message)
    {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
    }
}
